/** https://leetcode.com/problems/best-time-to-buy-and-sell-stock/ */

function maxProfit(prices: number[]): number {
    let max_profit = 0
    let min_sell_price = prices[0]
    let profit_per_day = [0]

    for (let i = 1; i < prices.length; i++) {
        let profit = Math.max(profit_per_day[i - 1], -min_sell_price + prices[i])
        profit_per_day.push(profit)
        if (min_sell_price > prices[i]) {
            min_sell_price = prices[i]
        }

        if (profit > max_profit) {
            max_profit = profit
        }
    }


    return max_profit
};