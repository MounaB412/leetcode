/** https://leetcode.com/problems/binary-search/ */

function search(nums: number[], target: number): number {
    let left = 0;
    let right = nums.length - 1

    while (left <= right) {
        let mid: number = Math.floor((left + right) / 2)
        if (nums[mid] < target) {
            left = mid + 1
        } else if (nums[mid] > target) {
            right = mid - 1
        } else {
            return mid
        }
    }

    return -1

};