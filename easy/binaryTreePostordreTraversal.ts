/** https://leetcode.com/problems/binary-tree-postorder-traversal/ */

function postorderTraversal(root: TreeNode | null): number[] {
    return postOrderrecursiveTraversal(root, [])
};

function postOrderrecursiveTraversal(root: TreeNode | null, postorder: number[]): number[] {
    if (!root) {
        return postorder
    }

    postorder = postOrderrecursiveTraversal(root.left, postorder)
    postorder = postOrderrecursiveTraversal(root.right, postorder)

    postorder.push(root.val)

    return postorder
}