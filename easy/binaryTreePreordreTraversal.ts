/** https://leetcode.com/problems/binary-tree-preorder-traversal/ */

class TreeNode {
    val: number
    left: TreeNode | null
    right: TreeNode | null
    constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
        this.val = (val===undefined ? 0 : val)
        this.left = (left===undefined ? null : left)
        this.right = (right===undefined ? null : right)
    }
}
   

function preorderTraversal(root: TreeNode | null): number[] {
    return preordreRecursiveTraversal(root, [])
};

function preordreRecursiveTraversal(root: TreeNode | null, preorder: number[]): number[] {
    if (!root) {
        return preorder
    }

    preorder.push(root.val)

    preorder = preordreRecursiveTraversal(root.left, preorder)

    preorder = preordreRecursiveTraversal(root.right, preorder)

    return preorder
}