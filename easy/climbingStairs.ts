/** https://leetcode.com/problems/climbing-stairs/ */

function climbStairs(n: number): number {
    let climbingPattern = [0, 1, 2]

    for (let i = 3; i <= n; i++) {
        climbingPattern.push(climbingPattern[i - 2] + climbingPattern[i - 1])
    }

    return climbingPattern[n]
};