/** https://leetcode.com/problems/fibonacci-number/ */

function fib(n: number): number {
    let k = 0
    let l = 1
    let fib_number = n == 0 ? 0 : 1

    for (let i = 2; i < n + 1; i++) {
       fib_number = k + l
       k = l
       l = fib_number
    }

    return fib_number
};