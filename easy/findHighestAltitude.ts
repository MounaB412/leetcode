/** https://leetcode.com/problems/find-the-highest-altitude/ */

function largestAltitude(gain: number[]): number {
    const altitudes = [0]

    for (let i = 1; i < gain.length + 1; i ++) {
        altitudes[i] = altitudes[i - 1] + gain[i - 1]

    }
    return Math.max(... altitudes)
};