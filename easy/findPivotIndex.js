/**https://leetcode.com/problems/find-pivot-index/ */

/**
 * @param {number[]} nums
 * @return {number}
 */
var pivotIndex = function(nums) {
    let sum = 0;
    nums.forEach((num) => {
        sum += num
    })
    let rightSum = sum;
    let leftSum = 0;

    for (let i = 0; i < nums.length; i++) {
        if (nums[i - 1]) {
            leftSum += nums[i - 1]
        }
        rightSum -= nums[i]

        if (leftSum == rightSum) {
            return i
        }
    }

    return -1

};