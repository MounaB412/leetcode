/** https://leetcode.com/problems/flood-fill/ */

let originalPixel: number
let floodingColor: number
let m: number
let n: number

function floodFill(image: number[][], sr: number, sc: number, color: number): number[][] {
    originalPixel = image[sr][sc]
    floodingColor = color
    m = image.length
    n = image[0].length

    floodRecursively(image, sr, sc)

    return image
};

function floodRecursively(image: number[][], i: number, j: number): void {
    if (i < 0 || j < 0 || i > m - 1 || j > n - 1 ) {
        return;
    }

    if (image[i][j] == floodingColor || image[i][j] != originalPixel) {
        return;
    }

    image[i][j] = floodingColor

    floodRecursively(image, i - 1, j)
    floodRecursively(image, i, j - 1)
    floodRecursively(image, i, j + 1)
    floodRecursively(image, i + 1, j)
}