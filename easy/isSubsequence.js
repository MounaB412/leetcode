/**https://leetcode.com/problems/is-subsequence/ */

/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function(s, t) {
    let indexToMatch = 0;
    let matched = false;

    if (s.length == 0) {
        return true
    }

    for (let i = 0; i < t.length; i++) {
        if (t[i] == s[indexToMatch]) {
            if (indexToMatch == (s.length - 1)) {
                matched = true;
                break;
            } else {
                indexToMatch++
            }
        }
    }

    return matched
};