/** 
 * https://leetcode.com/problems/isomorphic-strings/ */

/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function(s, t) {
    const map = new Map();

    for (let i = 0; i < s.length; i++) {
        let keys = Array.from(map.keys())
        let values = Array.from(map.values())

        if (!keys.includes(s[i]) && !values.includes(t[i])) {
            map.set(s[i], t[i])
        }
        
    }

    let keys = Array.from(map.keys())
    let values = Array.from(map.values())
    let transformedT = ""

    for (let i = 0; i < t.length; i++) {
        let index = values.indexOf(t[i])

        if (index != -1) {
            let letter = keys[index]
            transformedT += letter
        }
    }

    console.log(transformedT)

    return s == transformedT

};