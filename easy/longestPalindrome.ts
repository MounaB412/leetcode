/** https://leetcode.com/problems/longest-palindrome/ */

function longestPalindrome(s: string): number {
    let map = new Map()
    let longuest_palindrome = 0

    for (let i = 0; i < s.length; i++) {
        let occurences = map.has(s[i]) ? map.get(s[i]) + 1 : 1;
        map.set(s[i], occurences)
        if (occurences % 2 == 0) {
            longuest_palindrome += 2
            map.set(s[i], 0)
        }
    }

    if (Array.from(map.values()).includes(1)) {
        longuest_palindrome++
    }

    return longuest_palindrome
};