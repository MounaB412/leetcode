/** https://leetcode.com/problems/middle-of-the-linked-list/ */

function middleNode(head: ListNode | null): ListNode | null {
    let length = 0;
    let current = JSON.parse(JSON.stringify(head));

    while(current) {
        length++
        current = current.next
    }

    let middle = Math.floor(length/2)

    for(let i = 0; i < middle; i++) {
        head = head?.next || null
    }

    return head;

};