/** https://leetcode.com/problems/n-ary-tree-postorder-traversal/ */


class nAryNode {
    val: number
    children: nAryNode[]
    constructor(val?: number) {
        this.val = (val===undefined ? 0 : val)
        this.children = []
    }
}

let postorderArray: number[];

function postorder(root: nAryNode | null): number[] {
    postorderArray = []
    postorderRecursive(root)
    return postorderArray
};

function postorderRecursive(root: nAryNode | null): void {
    if (!root) {
        return;
    }

    root.children.forEach((child: nAryNode) => {
        postorderRecursive(child)
    })

    postorderArray.push(root.val)
}