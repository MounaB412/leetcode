/** https://leetcode.com/problems/n-ary-tree-preorder-traversal/ */

let preorderArray: number[];

function preorder(root: nAryNode | null): number[] {
    preorderArray = []
    preorderRecursive(root)
    return preorderArray
};

function preorderRecursive(root: nAryNode | null): void {
    if (!root) {
        return;
    }

    preorderArray.push(root.val)

    root.children.forEach((child: nAryNode) => {
        preorderRecursive(child)
    })
}