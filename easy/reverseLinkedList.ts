/** https://leetcode.com/problems/reverse-linked-list/ */

function reverseList(head: ListNode | null): ListNode | null {
    let current = head
    let previous = null
    let reversedList;

    while(current) {
        reversedList = new ListNode()
        reversedList.val = current.val
        reversedList.next = previous
        current = current.next
        previous = reversedList
    }

    return reversedList
};