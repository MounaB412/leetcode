/**Link: https://leetcode.com/problems/running-sum-of-1d-array/ */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var runningSum = function(nums: Array<number>) {
    let sums: Array<number> = []
    for (let i = 0; i < nums.length; i++) {
        let sum: number = nums[i]
        if (sums[i-1]) {
            sum+= sums[i-1]
        }
        sums.push(sum)
    }

    return sums
};