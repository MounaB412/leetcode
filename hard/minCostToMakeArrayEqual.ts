/** https://leetcode.com/problems/minimum-cost-to-make-array-equal/description/ */

function minCost(nums: number[], costs: number[]): number {
    let left = Math.min(... nums)
    let right = Math.max(... nums)
    let minCost;

    while(left < right) {
        let mid = Math.floor((left + right) / 2)
        
        const costA = computeCost(nums, costs, mid)
        const costB = computeCost(nums, costs, mid + 1)

        if (costA < costB) {
            right = mid
            if (minCost == undefined || costA < minCost) {
                minCost = costA
            }
        } else if (costA > costB) {
            left = mid + 1
            if (minCost == null || costB < minCost) {
                minCost = costB
            }
        }
    }

    return minCost || 0
};

function computeCost(nums, costs, midian): number {
    let cost = 0
    for (let i = 0; i < nums.length; i++) {
        cost += Math.abs(midian - nums[i]) * costs[i]
    }

    return cost
}