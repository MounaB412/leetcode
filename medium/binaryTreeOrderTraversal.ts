/** https://leetcode.com/problems/binary-tree-level-order-traversal/ */

let orderedList: number[][];
function levelOrder(root: TreeNode | null): number[][] {
    orderedList = []
    leverOrderRecursive(root, 0)
    return orderedList
};

function leverOrderRecursive(root: TreeNode | null, level: number): void {
    if (!root) {
        return
    }

    if (orderedList.length <  (level + 1)) {
        orderedList.push([])
    }

    orderedList[level].push(root.val)

    leverOrderRecursive(root.left, level + 1)
    leverOrderRecursive(root.right, level + 1)
}