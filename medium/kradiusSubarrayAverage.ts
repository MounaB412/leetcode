/** https://leetcode.com/problems/k-radius-subarray-averages/ */

function getAverages(nums: number[], k: number): number[] {
    const cumulativeSum: number[] = new Array(nums.length).fill(0)

    for (let i = 0; i <  nums.length; i ++) {
        cumulativeSum[i] = (cumulativeSum[i - 1] || 0) + nums[i]
    }

    const averages: number[] = new Array(nums.length).fill(0)
    
    for (let i = 0; i <  nums.length; i ++) {
        if (i < k || i >=  (nums.length - k )) {
            averages[i] = -1
            continue
        }

        averages[i] = Math.floor((cumulativeSum[i + k] - (cumulativeSum[i - (k + 1)] || 0)) / (k * 2 + 1));
    }

    return averages
};