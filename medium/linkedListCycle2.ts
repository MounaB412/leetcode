/** https://leetcode.com/problems/linked-list-cycle-ii/ */

function detectCycle(head: ListNode | null): ListNode | null {
    let cycleFound = false
    let slow = head;
    let fast = head;

    while (fast && fast.next && !cycleFound) {
        slow = slow.next
        fast = fast.next.next
        if (slow == fast) {
            cycleFound = true
        }
    }

    if (!cycleFound) {
        return null
    }

    while (head != slow) {
        head = head.next
        slow = slow.next
    }

    return head
};