/** https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/ */

function lowestCommonAncestor(root: TreeNode | null, p: TreeNode | null, q: TreeNode | null): TreeNode | null {
	return recursiveSearch(root, p, q)
};

function recursiveSearch(root: TreeNode | null, p: TreeNode | null, q: TreeNode | null): TreeNode | null {
    if (!root) {
        return null
    }

    if (p.val < root.val && q.val > root.val) {
        return root
    }

    if (p.val > root.val && q.val < root.val) {
        return root
    }

    if (p.val == root.val) {
        return p
    }

    if (q.val == root.val) {
        return q
    }

    let next = p.val < root.val && q.val < root.val ? root.left : root.right

    return recursiveSearch(next, p, q)
}