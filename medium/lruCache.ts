/** https://leetcode.com/problems/lru-cache/ */

class LRUCache {
    capacity: number;
    map = new Map();
    pile: number[] = []

    constructor(capacity: number) {
        this.capacity = capacity
    }

    get(key: number): number {
        if (this.map.has(key)) {
            this.updatePile(key)
            return this.map.get(key)
        }
        return -1
    }

    put(key: number, value: number): void {
        let keys = Array.from(this.map.keys())
        if (keys.length == this.capacity && !keys.includes(key)) {
            let lru = this.pile.pop()
            this.map.delete(lru)
        }

        this.map.set(key, value)
        this.updatePile(key)
    }

    removeFromPile(key: number): void {
        var index = this.pile.indexOf(key);
        if (index != -1) {
            this.pile.splice(index, 1)
        }
    }

    updatePile(key: number): void {
        this.removeFromPile(key)
        this.pile.unshift(key)
    }
}