/** https://leetcode.com/problems/number-of-islands/ */

function numIslands(grid: string[][]): number {
    let nIslands = 0;
    let m = grid.length
    let n = grid[0].length
    let visited: boolean[][] = [...Array(m)].map(_=>Array(n).fill(false))       

    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n ; j++) {
            if (grid[i][j] == "1" && !visited[i][j]) {
                dfs(grid, visited, i, j, m, n)
                nIslands ++
            }
        }
    }

    return nIslands
};

function dfs(grid: string[][], visited: boolean[][], i: number, j: number, m: number, n: number): void {
    if (i < 0 || j < 0 || i > m - 1|| j > n - 1) {
        return
    }
    
    if (visited[i][j]) {
        return
    }

    if (grid[i][j] == "0") {
        return
    }

    visited[i][j] = true

    dfs(grid, visited, i - 1, j, m, n)
    dfs(grid, visited, i, j - 1, m, n)
    dfs(grid, visited, i, j + 1, m, n)
    dfs(grid, visited, i + 1, j, m, n)
}