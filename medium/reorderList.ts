/** https://leetcode.com/problems/reorder-list/ */

function reorderList(head: ListNode | null): void {
    let reversedList = reverseList(head);
    let copy = head;
    let length = 0;

    while (copy) {
        length ++
        copy = copy.next
    }

    let cut = 0
    while (reversedList && head) {
        if (cut % 2 == 0) {
            let newNode = new ListNode()
            newNode.val = reversedList.val
            newNode.next = head.next

            head.next = newNode
            reversedList = reversedList.next
        }
        cut++

        if (cut == length) {
            head.next = null
        }
        
        head = head.next

    }
};

function reverseList(head: ListNode | null): ListNode | null {
    let current = head
    let previous = null
    let reversedList

    while(current) {
        reversedList = new ListNode()
        reversedList.val = current.val
        reversedList.next = previous
        current = current.next
        previous = reversedList
    }

    return reversedList
};