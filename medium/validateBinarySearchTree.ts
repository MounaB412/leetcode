/** https://leetcode.com/problems/validate-binary-search-tree/ */

let order: number[]

function isValidBST(root: TreeNode | null): boolean {
    order = []
    return orderTree(root)
};

function orderTree(root: TreeNode | null): boolean {
    if (!root) {
        return true;
    }

    let isLeftCorrect = orderTree(root.left)

    if (!isLeftCorrect) {
        return false
    }

    
    if (order.length && order[order.length - 1] >= root.val) {
        return false
    }

    order.push(root.val)

    let isRightCorrect = orderTree(root.right)

    return isRightCorrect
}